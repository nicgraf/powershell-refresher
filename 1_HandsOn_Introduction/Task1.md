# Hands On Lab 1

## Tooling

Well done, you've set up your environment with vscode.
Now you can start to play around with the PowerShell extension.

Open the file "Hands-On-1.ps1" in this folder and start reading the instructions in the comment section.