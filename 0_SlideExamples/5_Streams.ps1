# do not remove this line, instead execute the script step by step (F8)
return;

#########################################################

### SUCCESS Stream (1)
Write-Output "This is a normal String"
$val = Write-Output "I can be stored in a variable"
$val

#########################################################

### ERROR Stream (2)
Write-Error "Oh No, Something Happened"

# This will not work, variables only capture output to SUCCESS stream
$Nothing = Write-Error "This does not work!"
$Nothing

# This will work -> Redirection to SUCCESS stream
$errorRecord = Write-Error "This works" 2>&1
$errorRecord

#########################################################

### Warning Stream (3)
Write-Warning "Careful now. Dangerous step ahead!"

#########################################################

### Verbose Stream (4)
# The preference for verbose output is "SilentlyContinue"
# Verbose output is hidden
$VerbosePreference

# We can use Write-Verbose in the code without it showing in the end
Write-Verbose "This will not show"

# But we can enable the output if we want to:
$VerbosePreference = "Continue"
Write-Verbose "This will now show (for the current session)"


#########################################################

### DEBUG Stream (5)
# Debug Information is also hidden by default
# using the $DebugPreference Variable
$DebugPreference

Write-Debug "Executing somefunction()"

# Use this only to output extremely low level output in your scripts
# Very useful for debugging 
# -> "Calling function X()"
# -> "5+5 = 10"
# -> ...

#########################################################

### INFORMATION Stream (5)
# The information stream is very similar to the SUCCESS stream
# It is used to output messages to the user without interfering with the script output

# Lets define a simple function script with some messages (BAD!):
function Start-MyBadFunction {
    Write-Output "Hello my dear, thank you for executing this script."
    Write-Output "It will now show the contents of '/'"
    Get-ChildItem -Path "/"
    Write-Output "Done! See you next time"
} 
# Select the whole block and press F8 to load the function into PS

$output = Start-MyBadFunction

# Now look at the contents of the variable:
# That's not useful at all is it?
$output

# Side note: Interestingly, you get an array of 4 objects: 3x string and 1x the actual result
# Piping the object to "Get-Member" shows the type of the objects
$output | Get-Member

# If you really cannot do otherwise, you COULD just select the correct object(s) and move on.
$output[3..8]

# You've now seen that outputting messages in a script is a bad thing.
# But that's needed a lot of times, we cannot just print warning messages instead.

# Solution: use the INFORMATION Stream

$InformationPreference = "Continue" # this is also hidden by default
Write-Information "Information Stream message"
Write-Output "Not information Stream message"

# No difference visible, right?

####

# so let's try our function approach again, but different:
function Start-MyGoodFunction {
    # do not worry yet about this line (see NOTE below)
    [CmdLetBinding()]param()

    Write-Information "Hello my dear, thank you for executing this script."
    Write-Information "It will now show the contents of '/'"
    Get-ChildItem -Path "/"
    Write-Information "Done! See you next time"
}
# load the function again (Selection + F8)

# Now execute the function
$output = Start-MyGoodFunction

# See what happened? No more >/dev/null to every script.
# You can actually call the single function with the 
# -InformationAction parameter to toggle the output
$output = Start-MyGoodFunction -InformationAction SilentlyContinue

######## NOTE for advanced users:
# The -InformationAction -VerboseAction, etc Parameters are only available on 
# Compiled CmdLets (From C# source code)
# BUT, we can add the [CmdLetBinding()] attribute to the parameter block "param()".
# This will instruct powershell to allow these parameters, see also:
Get-Help about_functions_cmdletbindingattribute