Import-Module (Join-Path -Path $PSScriptRoot -childpath "z_CourseUsers.psm1") -Force

$PwdPolicy = Get-PasswordPolicy -MinLength 12 -MaxLength 14 -NoSymbols

# Only ask for credentials if not already asked
if (-not $script:AdminUser) {
    $script:AdminUser = Get-Credential -Message "Enter 4-EA Credentials"
}

# Initialize IAM Client
try {
    Initialize-IAMClient -Credentials $script:AdminUser
} catch {
    Write-Host $_
    return
}

# GENERATE USERS

$Users = New-CourseUsers -StartNumber 1 -NumberOfUsers 11 -PwdPolicy $PwdPolicy -UserPrefix "s4d-ps2-" -UserComment "S4D-PowerShell" -AdminUser $AdminUser -ParentPersona "ti12952"


$Group = "id-s4d-powershell-course-users"
Add-ETHGroupMember -Identity $Group -Members $Users.username

# ---------------    “S4D-PS-PG”


